terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "5.41.0"
    }
  }
}


  provider "aws" {
      region = "us-east-2"
      shared_config_files = [ "/root/.aws/config" ]
      shared_credentials_files = [ "/root/.aws/credentials" ]
  
 }

terraform {
  backend "s3" {
    bucket = "terraformmmm-01"
    key = "Terraform/terraform.tfstate"
    region ="us-east-2"
    dynamodb_table = "Terraform-01"   
  }
}
  # module "my-instance" {
  #    region = var.region
  #    source = "./EC2/"
  #    ami = var.ami
  #    instance_type = var.instance_type
  #    key_name = var.key_name

# }

 module "vpc" {
     source = "./VPC"
     region = "us-east-2"
     private_subnet_cidr = var.private_subnet_cidr
     public_subnet_cidr = var.public_subnet_cidr
     vpc_cidr = var.vpc_cidr
  
 }
# module "Terraform_sg" {
#   source = "./SECURITY_GRP"
#   vpc_id = module.vpc.vpc_id
# }

# module "web_server_sg" {
#     source = "./module/SECURITY_GRP"
#     Name = "terraform_sg"
#     vpc_id = "my_vpc"
#       ingress_rules = [
#     {
#       from_port   = 80
#       to_port     = 80
#       protocol    = "tcp"
#       cidr_blocks = ["0.0.0.0/0"]
#     },
#     {
#       from_port   = 443
#       to_port     = 443
#       protocol    = "tcp"
#       cidr_blocks = ["0.0.0.0/0"]
#     }
#   ]
# }


module "Terraform_sg" {
  source = "./SECURITY_GRP"
  vpc_id = module.vpc.vpc_id
}


module "alb" {
  source = "./Loadbalancer"
  name = var.name
  vpc_id = module.vpc.vpc_id
  instance_ids = module.instance.instance.id
  subnet_ids = ["module.vpc.public_subnet_2a"]
  security_group_ids = module.Terraform_sg.Terraform_sg.id
  protocol = var.protocol
  target_group_name = var.target_group_name
  Terraform_sg = module.Terraform_sg.Terraform_sg
 # Replace with your security group IDs
}


resource "aws_lb_target_group_attachment" "target_attachment_module" {
  count            = min(length(module.my_instances.instance_ids), 2)
  target_group_arn = module.alb.target_group_arn
  target_id        = module.my_instances.instance_ids[count.index]
}