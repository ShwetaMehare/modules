resource "aws_vpc" "my_vpc" {
    cidr_block = var.vpc_cidr
    enable_dns_hostnames = true

    tags = {
      Name = "terraform-vpc"
    }
  
}

resource "aws_subnet" "public-subnet" {
    cidr_block = var.public_subnet_cidr
    vpc_id = aws_vpc.my_vpc.id
    availability_zone = "us-east-2a"

    tags = {
      Name = "public-subnet-2"
    }
  
}


resource "aws_subnet" "private-subnet" {
    cidr_block = var.private_subnet_cidr
    vpc_id = aws_vpc.my_vpc.id
    availability_zone = "us-east-2c"

    tags = {
      Name = "private-subnet-2"
    }
  
}

resource "aws_route_table" "my-route" {
    vpc_id = aws_vpc.my_vpc.id

    tags = {
      Name = "private-route-table"
    }
  
}

resource "aws_route_table_association" "pub-sub" {
    route_table_id = aws_route_table.my-route.id
    subnet_id = aws_subnet.public-subnet.id
  
}

resource "aws_route_table_association" "private-sub" {
    route_table_id = aws_route_table.my-route.id
    subnet_id = aws_subnet.private-subnet.id
  
}


resource "aws_internet_gateway" "terraform-igw" {
    vpc_id = aws_vpc.my_vpc.id

    tags = {
      Name = "terraform-igw"
    }
  
}

resource "aws_route" "attach-ig-route" {
    route_table_id = aws_route_table.my-route.id
    gateway_id = aws_internet_gateway.terraform-igw.id
    destination_cidr_block = "0.0.0.0/0"
  
}
