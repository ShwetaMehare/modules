variable "region" {
    default = "us-east-2"
    description = "providing to my server region"  
}

variable "ami" {
    default = "ami-0b8b44ec9a8f90422"
    description = "providing to my image id for server creation"
  
}

variable "instance_type" {
    default = "t2.micro"
    description = "providing to the size for my instance"
  
}

variable "key_name" {
    default = "pair"
    description = "providing to key pair for my server"
  
}

# variable "region" {
#     default = "us-east-2"
  
# }

variable "vpc_cidr" {
    default = "10.1.0.0/16"
  
}

variable "private_subnet_cidr" {
    default = "10.1.1.0/24"
  
}
variable "public_subnet_cidr" {
    default = "10.1.2.0/24"
  
}


variable "name" {
  default = "Terraform-alb"
}

variable "protocol" {
  default = "HTTP"
}

variable "target_group_name" {
  default = "Terraform-target-group"
}